#!/bin/bash
#
# Script to build Bioinformatica na Escola web site on Linux
#
# Compiler: cheetah

export PYTHONPATH=$(pwd)/src
# Compile template files into html
cheetah fill --nobackup --idir src --odir public -R
# Copy media (images, css) to the build directory
cp -a media public
# Remove base and helper files
rm public/[[:upper:]]*
