:: make.bat
:: Compiles Bioinformatica na Escola (PT version) web site on Windows
@ECHO OFF
SET PYTHONPATH=C:\PYTHON27\Lib\;C:\ARQUIVO\Bio_na_Escola_WebPage\bioinformatica-na-escola\src\
SET PATH=%PATH%;C:\Python27\
CALL python C:\Cheetah-2.4.4\bin\cheetah fill --nobackup --idir src --odir build -R
CALL xcopy /Y /Q /S media build\media\
CALL del build\Base.html build\Util.html build\Menu.html