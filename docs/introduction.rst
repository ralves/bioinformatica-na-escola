************
Introduction
************

This is the documentation for the Bioinformática na Escola web site. It details
the organization of files and the build process that generates a working web
site.

The site consists of templates, which are processed by the Cheetah templating
system to create HTML files, and media files (css and images).
