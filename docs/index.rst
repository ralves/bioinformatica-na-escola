.. Bioinformática na Escola documentation master file, created by
   sphinx-quickstart on Wed Sep 12 18:35:28 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bioinformática na Escola's documentation!
====================================================

************
Introduction
************

This is the documentation for the Bioinformática na Escola web site. It details
the organization of files and the build process that generates a working web
site.

The site consists of templates, which are processed by the Cheetah templating
system to create HTML files, and media files (css and images).
